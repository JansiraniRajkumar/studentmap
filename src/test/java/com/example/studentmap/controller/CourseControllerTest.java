package com.example.studentmap.controller;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.studentmap.model.Course;
import com.example.studentmap.service.CourseService;


@RunWith(MockitoJUnitRunner.class)
public class CourseControllerTest {

	@InjectMocks
	CourseController courseController;

	@Mock
	CourseService courseService;

	@Test
	public void testGetAllProductsForPositive() {

		List<Course> courses = new ArrayList<Course>();
		Course course = new Course(1, "java", "Spring");
		courses.add(course);
		Course course1 = new Course(2, "java", "spring");
		courses.add(course1);
		Mockito.when(courseService.getallCourses()).thenReturn(courses);
		ResponseEntity<List<Course>> re = courseController.getAllCourses();
		Assert.assertNotNull(re);
		Assert.assertEquals(HttpStatus.OK, re.getStatusCode());

	}

	@Test
	public void testGetAllProductsForNegative() {

		List<Course> courses = new ArrayList<Course>();
		Course course = new Course(-1, "java", "Spring");
		courses.add(course);
		Course course1 = new Course(-2, "java", "spring");
		courses.add(course1);
		Mockito.when(courseService.getallCourses()).thenReturn(courses);
		ResponseEntity<List<Course>> re = courseController.getAllCourses();
		Assert.assertNotNull(re);
		Assert.assertEquals(HttpStatus.OK, re.getStatusCode());

	}

	@Test
	public void testCreateCourseForPositive() {
		Course course = new Course(1, "java", "Spring");
		Mockito.when(courseService.addCourse(course)).thenReturn(course);
		ResponseEntity<Course> course1 = courseController.addCourse(course);
		Assert.assertNotNull(course1);
		Assert.assertEquals(HttpStatus.OK, course1.getStatusCode());

	}

	@Test
	public void testCreateCourseForNegative() {
		Course course = new Course(-1, "java", "Spring");
		Mockito.when(courseService.addCourse(course)).thenReturn(course);
		ResponseEntity<Course> course1 = courseController.addCourse(course);
		Assert.assertNotNull(course1);
		Assert.assertEquals(HttpStatus.OK, course1.getStatusCode());

	}

	@Test
	public void testUpdateCourseForNegative() {
		Course course = new Course(-1, "java", "Spring");
		Mockito.when(courseService.updateCourse(course)).thenReturn(course);
		ResponseEntity<Course> course1 = courseController.updateCourse(course);
		Assert.assertNotNull(course1);
		Assert.assertEquals(HttpStatus.OK, course1.getStatusCode());

	}

	@Test
	public void testUpdateCourseForPositive() {
		Course course = new Course(1, "java", "Spring");
		Mockito.when(courseService.updateCourse(course)).thenReturn(course);
		ResponseEntity<Course> course1 = courseController.updateCourse(course);
		Assert.assertNotNull(course1);
		Assert.assertEquals(HttpStatus.OK, course1.getStatusCode());

	}

	@Test
	public void testDeleteCourseForNegative() {
		new Course(-1, "java", "spring");
		courseService.deleteCourse(-1);
		ResponseEntity<String> course1 = courseController.deleteCourse(-1);
		Assert.assertNotNull(course1);
		Assert.assertEquals(HttpStatus.OK, course1.getStatusCode());

	}

	@Test
	public void testDeleteCourseForPositive() {
		new Course(1, "java", "spring");
		courseService.deleteCourse(1);
		ResponseEntity<String> course1 = courseController.deleteCourse(1);
		Assert.assertNotNull(course1);
		Assert.assertEquals(HttpStatus.OK, course1.getStatusCode());

	}

}
